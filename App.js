import React, { Component } from "react";
import Users from "./Users";
import "./App.css";
import Util from "../../util";
// data
import { authors } from "./data/authors";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { list: authors, topList: [], search: "", size: 10 };
  }

  showTenMore = () => {
    this.setState({ size: this.state.size + 10 });
  };
  follow = (item) => {
    this.setState({ topList: [...this.state.topList, item] });
  };
  unFollow = (item) => {
    this.setState({
      topList: this.state.topList.filter((top) => top !== item),
    });
  };
  handleChange = (event) => {
    this.setState({ search: event.target.value });
  };

  render() {
    const myList = this.state.list
      .filter((item) =>
        Util.removeDiacritics(item.surname)
          .toLowerCase()
          .includes(Util.removeDiacritics(this.state.search).toLowerCase())
      )
      .map((item) => {
        return (
          <Users
            key={item.id}
            item={item}
            follow={() => this.follow(item)}
            unFollow={() => this.unFollow(item)}
            btnChange={
              !this.state.topList.find((topItem) => topItem.id === item.id)
            }
          />
        );
      });

    const myTopList = this.state.topList.map((item) => {
      return (
        <Users
          key={item.id}
          item={item}
          unFollow={() => this.unFollow(item)}
          btnChange={
            !this.state.topList.find((topItem) => topItem.id === item.id)
          }
        />
      );
    });

    var oComponent = (
      <div className="body" id="body">
        <div>
          <div>
            <section className="header-section">
              <div className="row">
                <div className="columns">
                  <header className="content-header content-header--props-right">
                    <h1>Sledovaní autoři</h1>
                  </header>
                </div>
              </div>
            </section>
            <section>
              <div className="row">
                <div className="columns">
                  <div>
                    <h2>Autoři, které sledujete</h2>

                    <div className="list-items-group">{myTopList}</div>
                  </div>
                  <br />
                  <div>
                    <h2>Autoři, které můžete sledovat</h2>

                    <form>
                      <label for="fltr">Filtr podle příjmení</label>
                      <input
                        type="search"
                        id="fltr"
                        className="max-width-400"
                        onChange={this.handleChange}
                        placeholder="Zadejte příjmení autora"
                        value={this.state.search}
                      />
                    </form>

                    {myList.splice(0, this.state.size)}
                    <button
                      type="button"
                      onClick={this.showTenMore}
                      className="button"
                    >
                      + Zobrazit dalších 10
                    </button>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    );

    return oComponent;
  }
}

export default App;
