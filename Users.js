import React from "react";

const Users = (props) => {
  return (
    <div key={props.id} className="list-item--article-6">
      <div className="l-item__txt-wrap">
        <a href="javascript:void(0);">
          {props.item.name} {props.item.surname}
        </a>
      </div>
      <div className="l-item__tools">
        {props.btnChange ? (
          <button onClick={props.follow} className="button button--hollow">
            Sledovať
          </button>
        ) : (
          <button onClick={props.unFollow} className="button button--hollow">
            Zrušiť
          </button>
        )}
      </div>
    </div>
  );
};
export default Users;
